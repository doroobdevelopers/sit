SIT-Lib
==========

A Python package (On-going effort) to connect with all internal systems/communication tools directly from the codebase.

## Installation

    pip install git+https://<git-user>@github.com/S-Intelligent-Technologies/sit.git

## Slack Integration

Post a message into your Slack project channel

    >>> from sit import Communicate
    >>> com = Communicate()
    >>> com.broadcast('random', 'frontend tester running', 'frontend-tester-bot')

## Mailgun Integration

Send an email from anywhere.

    >>> from sit import Communicate
    >>> mailer = Communicate()
    >>> ## to send HTML body emails, pass a 'html' param
    >>> mailer.shootmail('Zingo Bingo', ["zaidfarekh@gmail.com", "z.farekh@sit-mena.com"], 'html', 'HTML Support', '<h1>this is an html email</h1>')
    >>> ## to send raw text, pass a 'text' param
    >>> mailer.shootmail('Zingo Bingo', ["zaidfarekh@gmail.com", "z.farekh@sit-mena.com"], 'text', 'Raw Text', 'Some Text here....')

Send an email with attachments like the example above but use shootmail_attached instead of shootmail.

    >>> mailer.shootmail_attached('Foo Bar', ["n.bazadough@sit-mena.com"], 'html', 'Attachments Support', '<h1>this is an email with attachments</h1>', ["/absolute/path/to/test.jpg", "/absolute/path/to/test.txt"])

## Trello Integration

Create a card on trello

    >>> from sit import Pm
    >>> cards = Pm()
    >>> cards.trello('Card Title gos here...', 'Card description gos here....')
    >>> ## To boradcast to slack that a card has been added, 
    >>> ## pass a true value as a third param to the trello def.
    >>> ## It will broad cast to General by default. 
    >>> ## We need to refactor to send it to a specific channel...
    >>> cards.trello('readme.md', 'Card description gos here....', 'True')

Incase the oauth token for trello needs to be refereshed, follow the below steps and trello library instructions.

    ## Generate token from command line
    >>> from trello import util
    >>> os.environ["TRELLO_API_KEY"] = "4992ef6ad3c05b0e7f9cf3c791dfd2de"
    >>> os.environ["TRELLO_API_SECRET"] = "b1a0e3b11625b57346e8ad69c9d429563364b65cf36faa80c9f38967ff483bfe"
    >>> os.environ["TRELLO_EXPIRATION"] = "30"
    >>> token = util.create_oauth_token()

For now it writes to a static board, we have to re-factor to make the board and lists dynamically chosen.

To get the list of boards and lists:

    ## getting lists
    client = TrelloClient(
        api_key='4992ef6ad3c05b0e7f9cf3c791dfd2de',
        api_secret='b1a0e3b11625b57346e8ad69c9d429563364b65cf36faa80c9f38967ff483bfe',
        token='728da8de966f971c6e785d7f3f7b46b29b573443975b81adc4fbf65f0333a76a',
        token_secret='a0a32db2fc928a684769be669b0a4f33'
    )
    zingo = Board(client, '53c72b2aed202c4c421c8ff1', 'ProjectX')
    zingo.all_lists()
    [<List Back burner>, <List To Do>, <List Doing>, <List Done>]
    lists = zingo.all_lists()
    for list in lists:
      list.id
        u'53d98eed23c0d0ceea238bab'
        u'53c72b2aed202c4c421c8ff2'
        u'53c72b2aed202c4c421c8ff3'
        u'53c72b2aed202c4c421c8ff4'

## Real-Time Tracking events with InfluxDB and Grafana
This library has been built to track arbitrary events within the codebase/software. (users using a certain feature, emails that are being sent, server usage, exceptions etc....)

Main Usage
    from sit import Track

    stream = Track()
    streamed = stream.t('<Metric Name>', [<dict of cols>], [<dict of values>])

Example Usage (after installing the library (SIT), you can create this file to stream the sine wave to the main chart in Grafana)


    #!/usr/bin/python

    import json
    import math
    import requests
    import sys
    from time import sleep
    from sit import Track

    STATUS_MOD = 10

    n = 0
    while True:
        for d in range(0, 360):
            stream = Track()
            streamed = stream.t('sin', ['val'], [[math.sin(math.radians(d))]])
            if streamed is False:
                print 'Failed for some reason.... -- aborting.'
                sys.exit(1)
            n += 1
            sleep(1)
            if n % STATUS_MOD == 0:
                print '%d points inserted.' % n

To Check and draw your graphs, please navigate to http://sitanalytics.cloudapp.net/#/dashboard/db/semantic-intelligent-technologies-dev-streaming

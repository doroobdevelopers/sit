from setuptools import setup

setup(name='sit',
      version='0.4.1',
      description='The core company library',
      classifiers=[
        'Development Status :: 1 - Alpha',
        'License :: Internal use only',
        'Programming Language :: Python :: 2.7',
        'Topic :: Utility and communication library',
      ],
      keywords='An SIT utility library...',
      url='http://github.com',
      author='Zaid Farekh',
      author_email='z.farekh@sit-mena.com',
      license='SIT internal use only',
      packages=['sit'],
      install_requires=[
          'requests',
          'pyslack',
          'py-trello',
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      entry_points={
          'console_scripts': ['main_boradcast=sit.command_line:main'],
      },
      include_package_data=True,
      zip_safe=False)

import os

import slack
import slack.chat
import requests
import json

from trello import TrelloClient, Board, List



class Communicate:

    ## Main slack api auth token.
    slack.api_token = 'xoxp-2966017405-2966017407-3553476952-301ce3'

    def broadcast(self, channel, message, bot):
        slack.chat.post_message('#' + channel, message, username=bot)

    def shootmail(self, sender, recipient, type, subject, body):
        mailgun_api_key = 'key-c09351d1105ecddea893a4f0f18b4585'
        mailgun_sender_email = ' <mailgun@sandbox39783cb6ed33407292ad2b1d8cfd7f05.mailgun.org>'
        mailgun_send_api_url = "https://api.mailgun.net/v3/sandbox39783cb6ed33407292ad2b1d8cfd7f05.mailgun.org/messages"

        return requests.post(
            mailgun_send_api_url,
            auth=("api", mailgun_api_key),
            data={"from": sender + mailgun_sender_email,
                "to": recipient,
                "subject": subject,
                type: body})

    # TODO: combine shootmail and shootmail_attached into one method
    def shootmail_attached(self, sender, recipient, type, subject, body, file_path_list):
        mailgun_api_key = 'key-c09351d1105ecddea893a4f0f18b4585'
        mailgun_sender_email = ' <mailgun@sandbox39783cb6ed33407292ad2b1d8cfd7f05.mailgun.org>'
        mailgun_send_api_url = "https://api.mailgun.net/v3/sandbox39783cb6ed33407292ad2b1d8cfd7f05.mailgun.org/messages"

        files = []
        for file_path in file_path_list:
            files.append(("attachment", open(file_path)))

        return requests.post(
            mailgun_send_api_url,
            auth=("api", mailgun_api_key),
            data={"from": sender + mailgun_sender_email,
                "to": recipient,
                "subject": subject,
                type: body},
            files=files)


class Pm:

    def trello(self, card_name, card_description, slack_publish='false'):

        client = TrelloClient(
                api_key='4992ef6ad3c05b0e7f9cf3c791dfd2de',
                api_secret='b1a0e3b11625b57346e8ad69c9d429563364b65cf36faa80c9f38967ff483bfe',
                token='728da8de966f971c6e785d7f3f7b46b29b573443975b81adc4fbf65f0333a76a',
                token_secret='a0a32db2fc928a684769be669b0a4f33'
        )
        board_obj = Board(client, '53c72b2aed202c4c421c8ff1', 'ProjectX')
        card = List(board_obj, '53d98eed23c0d0ceea238bab', 'Testing')

        if slack_publish == 'true':
            publish = Communicate()
            # Adjust the broadcast message to fit the context....
            publish.broadcast('random', 'frontend tester running', 'frontend-tester-bot')

        card.add_card(card_name, card_description)

class Track:

    def t(self, name, columns, points):
        database = 'test1'
        main_url = 'http://sitanalytics.cloudapp.net:8086/db/%s/series?u=root&p=root' % database

        # Columns and points have to come in as dicts
        # @todo : snaitize dicts
        # @todo : allow inlfux tagging.
        # @todo : Add data retention policy -- Very high priority

        v = [{'name': name, 'columns': columns, 'points': points}]
        post_request = requests.post(main_url, data=json.dumps(v))
        if post_request.status_code != 200:
            # return 'Failed to add point to influxdb -- aborting.'
            return False
        else:
            return True
